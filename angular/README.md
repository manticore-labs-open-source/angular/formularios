# formularios

Este es un producto desarrollado y mantenido por Manticore Labs.

## Instalación

Descarga el repositorio y agrega las carpetas `estilos` y `componentes` en tu proyecto de `Angular`.

Para usarlo dentro de cualquier `modulo de angular` solo se debe importar el modulo `MlCampoFormularioModule`.

Ejemplo:

```typescript
import {MlCampoFormularioModule} from '../librerias-externas/manticore-labs/componentes/ml-campo-formulario/ml-campo-formulario.module';

@NgModule({
    declarations: [
        // Componentes
    ],
    imports: [
        // Modulos,
        MlCampoFormularioModule
    ],
    providers: [
        // Servicios
    ],
    bootstrap: [
        // Componente principal
    ]
})
export class AppModule {
}
```

También se debe agregar los estilos para que funcionen de manera correcta los formularios. Dirigirnos al archivo de
estilos principal `styles.scss`

```scss
// styles.scss
@import 'src/app/librerias-externas/manticore-labs/estilos/formularios';
```

## Ejemplo de Uso

En el componente donde se va a usar, por ejemplo `app.componente.ts`, vamos a crear una variable con los datos del
formulario. El formulario tiene algunos campos comunes y otros particulars para cada tipo de campo.

### Campo `text`

Vamos a empezar por definir un campo normal de tipo texto. Este archivo tiene el nombre `campo-correo.ts`. Una copia de
esto se encuentra dentro del repositorio. A continuación se encuentra el archivo y un comentario según cada uno de los
campos:

```typescript
// campo-correo.ts
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';


export const CAMPO_CORREO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    // La clase componente sirve para poder utilizar cualquier variable dentro
    // del componente donde se está usando este formulario
    // EJ:
    // claseComponente.servicio
    // claseComponente.propiedadUno
    // claseComponente.metodoUno()
    return {
        hidden: false, // permite que el campo no se renderice. Si seleccionas ésta opción no se valida el campo
        componente: claseComponente, // componente donde se esta utilizando el formulario
        validators: [ // arreglo de validaciones de Angular, se puede usar cualquiera
            Validators.required,
            // Validators.email 
        ],
        asyncValidators: null, // TODO: sirve para validaciones asincronas pero no está aún implementado
        valorInicial: 'info', // valor inicial del campo (se puede modificar programaticamente)
        nombreCampo: 'correo', // nombre de la variable y formControlName del campo
        nombreMostrar: 'Correo', // nombre para mostrarse al usuario
        textoAyuda: 'Ingresa tu correo.',  // texto de ayuda ubicado en la parte inferior, también es el title
        placeholderEjemplo: 'Ej: info@manticore-labs.com', // placeholder
        formulario: {}, // inicialiar vacio por temas de Lint
        mensajes: MENSAJES_ERROR(claseComponente), // Mensajes dependiendo el error que se tiene. Ej: required, minlength, etc.
        parametros: { // los parametros ayudan a llenar los mensajes de error
            nombreCampo: 'correo', // El nombre a mostrars en los errores, hacen referencia al nombre del campo.
            // minlength: 10, 
            // maxlength: 20,
            // min: 1,
            // max: 2,
            // mensajePattern: 'Ingresa un patrón correctamente',
        },
        estaValido: false, // inicializar en falso
        tipoCampoHtml: 'text', // 'password' | 'text' | 'select' | 'inputMask' | 'inputNumber' | 'inputSwitch' | 'autocomplete'
        valorActual: '', // valor con el que inicia el formControl
        tamanioColumna: 6, // se usa columnas de bootstrap, puedes elegir el número de la columna
        disabled: false // Si el campo está deshabilitado. Si seleccionas ésta opción no se valida el campo
    };
};
```

### Campo `password`

Este archivo tiene el nombre `campo-password.ts`. Una copia de esto se encuentra dentro del repositorio. Se utilizan los
mismos campos de tipo `texto` pero se debe de usar el `tipoCampoHtml` con valor `password`:

```typescript
// campo-password.ts`
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_PASSWORD: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        hidden: false,
        componente: claseComponente,
        validators: [
            Validators.required,
            Validators.minLength(6)
        ],
        asyncValidators: null,
        valorInicial: '',
        nombreCampo: 'password',
        nombreMostrar: 'Password',
        textoAyuda: 'Ingresa tu password.',
        placeholderEjemplo: 'Ej: ***********',
        formulario: {},
        mensajes: MENSAJES_ERROR(claseComponente),
        parametros: {
            nombreCampo: 'Password',
            minlength: 6,
        },
        estaValido: false,
        tipoCampoHtml: 'password',
        valorActual: '',
        tamanioColumna: 6,
        disabled: false
    };
};
```

### Campo `select`

**Importante**, de aquí en adelante se van a omitir los `campos comunes` que son los campos que se san en los
campos `text` y `password`. Este archivo tiene el nombre `campo-ciudad.ts`. Se debe de usar el `tipoCampoHtml` con
valor `select`:

```typescript
// campo-ciudad.ts`
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CIUDAD: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        // ... Aqui los otros campos
        select: {
            valoresSelect: [
                {nombre: 'Quito', id: 1},
                {nombre: 'Guayaquil', id: 1}
            ], // opciones a mostrarse, puede utilizarse una variable del componente
            // valoresSelect: claseComponente.miArreglo
            placeholderFiltro: 'Filtre por nombre', // Placeholder para el filtro de busqueda
            mensajeFiltroVacio: 'No se encontró', // Mensaje cuando el filtro esta vacio
            campoFiltrado: 'nombre', // campo para filtrar
            fnMostrarEnSelect: (campo: any) => { // funcion para mostrar texto en la opcion del select
                return campo.nombre
            },
            campoSeleccionado: 'nombre' // Campo que se muestra al seleccionar un item
        }
    };
};
```

### Campo `inputMask`

**Importante**, de aquí en adelante se van a omitir los `campos comunes` que son los campos que se san en los
campos `text` y `password`. Este archivo tiene el nombre `campo-cedula.ts`. Se debe de usar el `tipoCampoHtml` con
valor `inputMask`:

```typescript
// campo-cedula.ts`
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CEDULA: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        // ... Aqui los otros campos
        inputMask: {
            mask: '99-99999999', // mask https://primefaces.org/primeng/showcase/#/inputmask
            slotChar: '_', // slotChar https://primefaces.org/primeng/showcase/#/inputmask
            characterPattern: [],  // characterPattern https://primefaces.org/primeng/showcase/#/inputmask
            fnValidar: (campo, componente) => { // validacion para saber si el campo está correcto o no
                const numeros = campo.replaceAll('_', '').replaceAll('-', '');
                return numeros.length >= 10;
            }
        }
    };
};
```

### Campo `inputNumber`

**Importante**, de aquí en adelante se van a omitir los `campos comunes` que son los campos que se san en los
campos `text` y `password`. Este archivo tiene el nombre `campo-sueldo.ts`. Se debe de usar el `tipoCampoHtml` con
valor `inputNumber`:

```typescript
// campo-sueldo.ts`
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_SUELDO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        // ... Aqui los otros campos
        inputNumber: {
            max: 100, // valor máximo
            min: 0, // valor minimo
            prefix: '$  ', // prefijo
            suffix: ' USD', // sufijo
            minFractionDigits: 4, // número de decimales
            fnValidar: (campo, componente) => { // validación customizadas
                return true; // se puede usar el componente
            }
        }
    };
};
```

### Campo `inputSwitch`

**Importante**, de aquí en adelante se van a omitir los `campos comunes` que son los campos que se san en los
campos `text` y `password`. Este archivo tiene el nombre `campo-casado.ts`. Se debe de usar el `tipoCampoHtml` con
valor `inputSwitch`:

```typescript
// campo-casado.ts`
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CASADO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        // ... Aqui los otros campos
        valorInicial: true, // true o false
    };
};
```

### Campo `autocomplete`

**Importante**, de aquí en adelante se van a omitir los `campos comunes` que son los campos que se san en los
campos `text` y `password`. Este archivo tiene el nombre `campo-autocomplete.ts`. Se debe de usar el `tipoCampoHtml` con
valor `autocomplete`:

```typescript
// campo-autocomplete.ts`
import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_AUTOCOMPLETE: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        // ... Aqui los otros campos
        autocomplete: {
            suggestions: [], // suggestions https://primefaces.org/primeng/showcase/#/autocomplete
            field: 'title', // campo a mostrarse al seleccionar
                            // se puede usar '{{a.nombre + ' ' + b.apellido}}' 
                            // field https://primefaces.org/primeng/showcase/#/autocomplete
            delay: 2000, // delay https://primefaces.org/primeng/showcase/#/autocomplete
            inputId: 'userId', // inputId https://primefaces.org/primeng/showcase/#/autocomplete
            emptyMessage: 'No hay registros', // emptyMessage https://primefaces.org/primeng/showcase/#/autocomplete
            fnMostrarEnAutoComplete: (campo) => { // funcion para mostrar texto en el autocomplete
                return campo.title;
            }
        },  
    };
};
```

## Funciones

Existen algunas funciones que ayudan a realizar diferentes

### Aplicar filtro

Esta funcion ayuda a filtrar un arreglo dependiendo un arreglo de campos que enviemos

`aplicarFiltro`

Ejemplo de uso

```typescript
// campo-autocomplete.ts`

@Component({
    selector: 'app-componente',
    templateUrl: './componente.component.html',
    styleUrls: ['./componente.component.scss']
})
export class ComponenteComponent implements OnInit {
    constructor() {
    }

    ngOnInit(): void {
    }

    aplicarFiltro() {
        return aplicarFiltro(
            'vi', // cadena de búsqueda
            ['nombre', 'id'], // se puede filtrar por nombre o id
            [{nombre: 'Adrian', id: 1}, {nombre: 'Vicente', id: 2}],
            [{nombre: 'Adrian', id: 1}, {nombre: 'Vicente', id: 2}]
        ); // Resultado - [{nombre: 'Vicente', id: 2}]
    }
}
```

