import {FormularioErrorMensaje} from '../../componentes/ml-error-form/interfaces/formulario-error-mensaje';

export const MENSAJES_ERROR: (clase: any) => FormularioErrorMensaje[] =
  (claseComponente: any) => {
    return [
      {
        tipo: 'required',
        mensaje: (parametros) => {
          return `${parametros.nombreCampo} es requerido`;
        }
      },
      {
        tipo: 'minlength',
        mensaje: (parametros) => {
          console.log(parametros);
          return `${parametros.nombreCampo} debe ser mayor a ${parametros.minlength}`;
        }
      },
      {
        tipo: 'maxlength',
        mensaje: (parametros) => {
          return `${parametros.nombreCampo} debe ser menor a ${parametros.maxlength}`;
        }
      },
      {
        tipo: 'min',
        mensaje: (parametros) => {
          return `${parametros.nombreCampo} debe ser mayor a ${parametros.min}`;
        }
      },
      {
        tipo: 'max',
        mensaje: (parametros) => {
          return `${parametros.nombreCampo} debe ser menor a ${parametros.max}`;
        }
      },
      {
        tipo: 'email',
        mensaje: (parametros) => {
          return `${parametros.nombreCampo} debe ser un correo válido.`;
        }
      },
      {
        tipo: 'pattern',
        mensaje: (nombreCampo) => {
          return `${nombreCampo} debe de cumplir con ${nombreCampo.mensajePattern}`;
        }
      }
    ];
  };
