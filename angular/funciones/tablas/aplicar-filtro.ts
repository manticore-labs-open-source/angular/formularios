export function aplicarFiltro(
    valorFiltro: string,
    nombreCampo: string[],
    arregloDatos: any[],
    arregloDatosFiltrado: any[],
) {
    const arregloClonado = Object.assign(arregloDatos) as any;
    if (valorFiltro !== '') {
        arregloDatosFiltrado = arregloClonado
            .filter(
                (documento: any) => {
                    const encontroMatch = nombreCampo.some(
                        (nC) => documento[nC]?.toString().toLowerCase().includes(valorFiltro.toLowerCase())
                    );
                    return encontroMatch;
                }
            );
    } else {
        arregloDatosFiltrado = arregloClonado;
    }
    return arregloDatosFiltrado;
}
