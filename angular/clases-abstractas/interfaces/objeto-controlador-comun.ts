export interface ObjetoControladorComun {
  modificarVigencia: {
    dto: any;
    mensaje: string;
  };
  crearEntidad: {
    dto: any;
    mensaje: string;
  };
  actualizarEntidad: {
    dto: any;
    mensaje: string;
  };
  busqueda: {
    dto: any;
    mensaje: string;
  };
}
