import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ActivoInactivo} from '../enums/activo-inactivo';
import {plainToClass} from 'class-transformer';

export abstract class ServicioComun<Entidad, BusquedaDto> {

  url = environment.url;
  nombreCampoVigencia = '';
  nombreIdentificador = '';

  constructor(
    protected readonly _path: string,
    private readonly httpClient: HttpClient,
    protected readonly busquedaDto: any,
    nombreCampoVigencia: string,
    nombreIdentificador: string,
  ) {
    this.nombreCampoVigencia = nombreCampoVigencia;
    this.nombreIdentificador = nombreIdentificador;
    this.url = this.url + '/' + _path;
  }

  crear(registro: Entidad): Observable<Entidad> {
    return this.httpClient
      .post(this.url, registro)
      .pipe(
        map(r => r as Entidad)
      );

  }

  actualizar(registro: Entidad, idRegistro: number): Observable<void> {
    const urlActualizar = this.url + '/' + idRegistro;
    return this.httpClient
      .put(urlActualizar, registro)
      .pipe(
        map(r => undefined)
      );
  }

  buscar(parametrosBusqueda: BusquedaDto): Observable<[Entidad[], number]> {
    const objeto = plainToClass(
      this.busquedaDto as any,
      parametrosBusqueda,
      {
        excludeExtraneousValues: true,
      }
    );
    const consulta = this.construirQueryParams(objeto as BusquedaDto);
    return this.httpClient
      .get(this.url + '?' + consulta)
      .pipe(
        map(r => r as [Entidad[], number])
      );
  }

  setearVigencia(valorVigencia: ActivoInactivo, idRegistro: number): Observable<void> {
    const urlActualizar = this.url + '/' + idRegistro + '/modificar-vigencia';
    let objeto: any = {};
    objeto[this.nombreCampoVigencia] = valorVigencia;
    return this.httpClient
      .put(urlActualizar, objeto)
      .pipe(
        map(r => undefined)
      );
  }

  protected construirQueryParams(objeto: BusquedaDto): string {
    const llaves: string[] = Object.keys(objeto);
    const objetoAny = objeto as any;
    let consulta = '';

    const llavesFiltradas = llaves
      .filter((a) => objetoAny[a] !== undefined);
    llavesFiltradas
      .forEach(
        (llave: string, indice: number) => {
          if (objetoAny[llave]) {
            const and = indice === llavesFiltradas.length - 1 ? '' : '&';
            consulta = consulta + `${llave}=${objetoAny[llave].toString()}${and}`;
          }
        }
      );
    return consulta;
  }

}
