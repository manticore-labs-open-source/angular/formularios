import { IsNumberString, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export abstract class BusquedaComunDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  skip?: number;

  @IsOptional()
  @IsNumberString()
  @Expose()
  take?: number;
}
