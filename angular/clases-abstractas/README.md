# Angular

Este es un producto desarrollado y mantenido por Manticore Labs. Gracias Angular, Class Validator y Class Transformer por sus excelentes herramientas de trabajo.

## Clases abstractas

### Servicio común

Para utilizar el servicio común se debe de implementarlo de la siguiente manera:

```typescript
import {Injectable} from '@angular/core';
import {ServicioComun} from '../../clases-abstractas/servicio-comun';
import {HttpClient} from '@angular/common/http';
import {AtributoInterface} from '../../interfaces/catalogo-atributo.interface';
import {AtributoBusquedaDto} from '../../dto/catalogo-atributo-busqueda.dto';

@Injectable()
export class AtributoService
    extends ServicioComun<AtributoInterface, AtributoBusquedaDto> {

    constructor(
        private readonly _httpClient: HttpClient,
    ) {
        super(
            'catalogo-atributo', // url
            _httpClient, // cliente http de angular
            CatalogoAtributoBusquedaDto, // Busqueda DTO
            'catVigencia', // nombre campo vigencia
            'idCatalogAtri' // nombre campo identificador
        );
    }
}
```

El servicio común permite el uso de 4 métodos importantes que son: `crear`, `actualizar`, `buscar` y `setearVigencia`

#### Crear

El método `crear` recibe un objeto de la Entidad que se va a modificar y usa el método http `POST` a la url definida en el servicio.

```typescript
// ejemplo de uso
const objeto:Entidad = {
    nombre: 'Adrian',
    nombreEmpresa: 'Manticore Labs'
}
this.servicio
    .crear( // la url sería: http://dominio:puerto/nombre-servicio POST
        objeto  
    )
    .subscribe(
        // Implementación
    )
```

#### Actualizar

El método `actualizar` recibe un objeto de la Entidad que se va a modificar, el identificador de la Entidad y usa el método http `PUT` a la url definida en el servicio.

```typescript
// ejemplo de uso
const objetoEntidadModificado = {
    nombre: 'Vicente',
}
this.servicio
    .actualizar( // la url sería: http://dominio:puerto/nombre-servicio/5 PUT
    '5', // Identificador
    objetoEntidadModificado // Objeto
    )
    .subscribe(
        // Implementación
    );
```

#### Modificar Vigencia

El método `modificarVigencia` recibe un objeto de la Entidad que se va a modificar con el nombre del campo Vigencia. El campo `vigencia` hace referencia al campo que `habilita o inhabilita` al registro (es como `eliminarlo` o `esconderlo` o `desactivarlo` sin quitarlo definitivamente de la base). El segundo parametro es el identificador de la Entidad y usa el método http `PUT` a la url definida en el servicio.

```typescript
// ejemplo de uso
const objetoVigencia = {
    habilitado: true,
}
this.servicio
    .setearVigencia( // la url sería: http://dominio:puerto/nombre-servicio/5 PUT
    '5', // Identificador
    objetoVigencia // Objeto
);
```

#### Buscar

El método `buscar` recibe una consulta de Typeorm, este objeto es FindManyOptions. Más
información [Aquí](https://typeorm.io)

```typescript
// ejemplo de uso
const busqueda: FindManyOptions<AtributoEntity> = {
    take: 10,
    skip: 20,
    where: [
        {
            nombre: Like("%dri%"),
            habilitado: true
        },
        {
            nombreEmpresa: Like("%dri%"),
            habilitado: true
        }
    ]
}
this.servicio.buscar(busqueda);
```

### Búsqueda común DTO

Este DTO nos ayuda a crear un objeto de transferencia de búsqueda que al menos tenga skip y take para la paginación de
registros. Se recomienda también tener un campo de ordenamiento.

### Controlador Común

El controlador común se lo debe de instanciar de la siguiente manera:

```typescript
import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {AtributoService} from './atributo.service';
import {ControladorComun} from '../clases-abstractas/controlador-comun';
import {AtributoVigenciaDto} from './dto/atributo-vigencia.dto';
import {ATRIBUTO_OCC} from './constantes/atributo.occ';
import {AuditoriaService} from '../servicios/auditoria/auditoria.service';
import {AtributoCrearDto} from './dto/atributo-crear.dto';
import {AtributoActualizarDto} from './dto/atributo-actualizar.dto';
import {SeguridadGuard} from '../guards/seguridad/seguridad.guard';
import {FindConditions, FindManyOptions, Like} from 'typeorm';
import {AtributoEntity} from './atributo.entity';
import {AtributoBusquedaDto} from './dto/atributo-busqueda.dto';

@Controller('atributo')
export class AtributoController extends ControladorComun {
    constructor(
        private readonly _atributoService: AtributoService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _atributoService,
            _auditoriaService,
            ATRIBUTO_OCC(
                AtributoVigenciaDto,
                AtributoCrearDto,
                AtributoActualizarDto,
                AtributoBusquedaDto,
            ),
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(@Query() parametrosConsulta: AtributoBusquedaDto, @Req() request) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            let consulta: FindManyOptions<AtributoEntity> = {
                where: [],
            };
            consulta = this.setearSkipYTake<AtributoEntity>(
                consulta,
                parametrosConsulta.skip,
                parametrosConsulta.take,
            );
            let consultaWhere = consulta.where as FindConditions<AtributoEntity>[];
            consulta.where = this.setearFiltro(
                'atrVigencia',
                consultaWhere,
                parametrosConsulta.atrVigencia,
            );
            consultaWhere = consulta.where as FindConditions<AtributoEntity>[];
            consulta.where = this.setearFiltro(
                'tipoAtributo',
                consultaWhere,
                parametrosConsulta.tipoAtributo,
            );
            consulta.relations = ['tipoAtributo', 'catalogoAtributo'];
            return this._atributoService.buscar(consulta);
        }
    }
}
```

Es muy importante los objetos que se deben de definir para poder hacer uso correcto y validado de los métodos del
servicio común. En este caso necesitamos definir las siguientes variables:

- `Objeto de Controlador Común OCC`

Este objeto nos permite definir mensajes de error al crear, actualizar, modificar vigencia o buscar.

```typescript
import {ObjetoControladorComun} from '../../clases-abstractas/interfaces/objeto-controlador-comun';

export const ATRIBUTO_OCC: (
    modificarVigenciaDto,
    crearDto,
    actualizarDto,
    busquedaDto,
) => ObjetoControladorComun = (
    modificarVigenciaDto,
    crearDto,
    actualizarDto,
    busquedaDto,
) => {
    const nombre = 'Atributo';
    return {
        modificarVigencia: {
            dto: modificarVigenciaDto,
            mensaje: 'Error modificando ' + nombre,
        },
        crearEntidad: {
            dto: crearDto,
            mensaje: 'Error creando ' + nombre,
        },
        actualizarEntidad: {
            dto: actualizarDto,
            mensaje: 'Error actualizando ' + nombre,
        },
        busqueda: {
            dto: busquedaDto,
            mensaje: 'Error buscando ' + nombre,
        },
    };
};
```

- `Crear dto`

Este objeto nos permite definir validaciones al momento de crear un nuevo registro.

```typescript
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {IsIn, IsString, IsNotEmpty, IsNumber} from 'class-validator';
import {Expose} from 'class-transformer';

export class AtributoCrearDto {
    @IsNotEmpty()
    @IsNumber()
    @Expose()
    orden: number;

    @IsNotEmpty()
    @IsString()
    @Expose()
    nombre: string;

    @IsNotEmpty()
    @IsInt()
    @Expose()
    numeroAtributos: number;
}
```

- `Actualizar dto`

Este objeto nos permite definir validaciones al momento de actualizar un registro.

```typescript
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {IsInt, IsString, IsOptional, IsNumber} from 'class-validator';
import {Expose} from 'class-transformer';

export class AtributoActualizarDto {
    @IsOptional()
    @IsNumber()
    @Expose()
    orden?: number;

    @IsOptional()
    @IsString()
    @Expose()
    nombre?: string;

    @IsOptional()
    @IsInt()
    @Expose()
    numeroAtributos?: number;
}
```

- `Vigencia dto`

Este objeto nos permite definir validaciones al momento de actualizar la vigencia de un registro.

```typescript
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {IsBoolean, IsNotEmpty} from 'class-validator';
import {Expose} from 'class-transformer';

export class AtributoVigenciaDto {
    @IsNotEmpty()
    @IsBoolean()
    @Expose()
    habilitado: boolean;
}
``` 

- `Busqueda dto`

Este objeto nos permite definir validaciones al momento de buscar un registro.

```typescript
import {IsNotEmpty, IsOptional, IsString} from 'class-validator';
import {Expose} from 'class-transformer';
import {ActivoInactivo} from '../../enums/activo-inactivo';
import {BusquedaComunDto} from '../../clases-abstractas/busqueda-comun-dto';

export class AtributoBusquedaDto extends BusquedaComunDto {
    @IsNotEmpty()
    @IsString()
    @Expose()
    nombre: string;
}
``` 

- `Guard de Seguridad`

Es necesario el usa de un Guard de seguridad para todos los metodos. Recomendamos crear un guard e implementarlo
modificando el archivo `controlador-comun.ts`.

`seguridad.guard.ts`.

```typescript
import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {SeguridadService} from '../../servicios/seguridad/seguridad.service';

@Injectable()
export class SeguridadGuard implements CanActivate {
    constructor(private readonly _seguridadService: SeguridadService) {
    }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request: any = context.switchToHttp().getRequest();
        return this._seguridadService.validarPorRol(request);
    }
}
``` 

`seguridad.service.ts`.

```typescript
import {Injectable} from '@nestjs/common';

@Injectable()
export class SeguridadService {
    // constructor() {} // seguridad imports

    validarPorRol(request: any) {
        if (request) {
            return true; // Logica de negocio
        } else {
            return false; // Logica de negocio
        }
    }
}
``` 


#### Controlador Común - USO

Lo único que se implementa manualmente es la búsqueda dentro del controlador, lo demás estaría listo:


```typescript
import {
    Controller,
    Get,
    HttpCode,
    Query,
    Req,
    UseGuards,
} from '@nestjs/common';
import {AtributoService} from './atributo.service';
import {ControladorComun} from '../clases-abstractas/controlador-comun';
import {AtributoVigenciaDto} from './dto/atributo-vigencia.dto';
import {ATRIBUTO_OCC} from './constantes/atributo.occ';
import {AuditoriaService} from '../servicios/auditoria/auditoria.service';
import {AtributoCrearDto} from './dto/atributo-crear.dto';
import {AtributoActualizarDto} from './dto/atributo-actualizar.dto';
import {SeguridadGuard} from '../guards/seguridad/seguridad.guard';
import {FindConditions, FindManyOptions} from 'typeorm';
import {AtributoEntity} from './atributo.entity';
import {AtributoBusquedaDto} from './dto/atributo-busqueda.dto';

@Controller('atributo')
export class AtributoController extends ControladorComun {
    constructor(
        private readonly _atributoService: AtributoService,
        private readonly _auditoriaService: AuditoriaService,
    ) {
        super(
            _atributoService,
            _auditoriaService,
            ATRIBUTO_OCC(
                AtributoVigenciaDto,
                AtributoCrearDto,
                AtributoActualizarDto,
                AtributoBusquedaDto,
            ),
        );
    }

    @Get('')
    @HttpCode(200)
    @UseGuards(SeguridadGuard)
    async obtener(@Query() parametrosConsulta: AtributoBusquedaDto, @Req() request) {
        const respuesta = await this.validarBusquedaDto(
            request,
            parametrosConsulta,
        );
        if (respuesta === 'ok') {
            let consulta: FindManyOptions<AtributoEntity> = {
                where: [],
            };
            /*
            * IMPLEMENTACIÓN
            * */
            return this._atributoService.buscar(consulta);
        }
    }
}
``` 

