import {Observable, of} from 'rxjs';
import {TAKE} from '../constantes/numero-registros-en-tabla/take';
import {aplicarFiltro} from '../librerias-externas/manticore-labs/funciones/tablas/aplicar-filtro';
import {ActivatedRoute} from '@angular/router';
import {map, mergeMap} from 'rxjs/operators';
import {ServicioComun} from './servicio-comun';
import {CargandoService} from '../servicios/cargando/cargando.service';
import {NotificacionService} from '../servicios/notificacion/notificacion.service';
import {ActivoInactivo} from '../enums/activo-inactivo';
import {MatDialog} from '@angular/material/dialog';
import {ModalCrearEditarComunComponent} from '../modal/modal-crear-editar-comun/modal-crear-editar-comun.component';

// <Entidad, Servicio>
export abstract class RutaComun<Parametros, BusquedaDto, Servicio, Entidad> {

  // // Datos de tabla
  arregloDatos: Entidad[] = [];
  arregloDatosFiltrado: Entidad[] = [];
  filtroGeneralTabla = '';
  camposFormulario: any = [];

  // Paginacion
  take = TAKE;
  skip = 0;
  totalRecords = 0;

  // parametros de ruta y busqueda
  parametros?: Parametros;
  busquedaDto?: BusquedaDto;

  constructor(
    private readonly servicio: ServicioComun<Entidad, BusquedaDto>,
    private readonly cargandoService: CargandoService,
    private readonly notificacionService: NotificacionService,
    public matDialog: MatDialog,
    camposFormulario?: any,
    public tituloCrear?: string,
    public descripcionCrear?: string,
    public tituloActualizar?: string,
    public descripcionActualizar?: string,
  ) {
    if (camposFormulario) {
      this.camposFormulario = camposFormulario;
    }
  }

  obtenerYGuardarParametrosRuta(activatedRoute: ActivatedRoute): Observable<Parametros> {

    return activatedRoute
      .queryParams
      .pipe(
        mergeMap(
          (parametrosConsulta: any) => {
            if (activatedRoute
              .parent) {
              return activatedRoute
                .parent
                .params
                .pipe(
                  map(
                    (parametrosRutaPapa: any) => {
                      return {
                        ...parametrosConsulta,
                        ...parametrosRutaPapa,
                      };
                    }
                  )
                );
            } else {
              return of({
                ...parametrosConsulta
              });
            }
          }
        ),
        mergeMap(
          (parametrosRutaTotales: any) => {
            return activatedRoute
              .params
              .pipe(
                map(
                  (parametrosRuta: any) => {
                    const parametros = {
                      ...parametrosRutaTotales,
                      ...parametrosRuta
                    } as Parametros;
                    this.parametros = parametros;
                    console.log('parametros', this.parametros);
                    return parametros;
                  }
                )
              );
          }
        )
      );
  }

  abrirModal(
    componente: any,
    registro?: Entidad,
  ): void {
    const dialogRef = this.matDialog.open(
      ModalCrearEditarComunComponent,
      {
        data: {
          componente,
          registro,
          camposFormulario: this.camposFormulario,
          servicio: this.servicio,
          cargandoService: this.cargandoService,
          notificacionService: this.notificacionService,
          tituloCrear: this.tituloCrear,
          descripcionCrear: this.descripcionCrear,
          tituloActualizar: this.tituloActualizar,
          descripcionActualizar: this.descripcionActualizar,
        }
      }
    );

  }
  activarDesactivar(evento: any,
                    idRegistro: number,
                    habilitado: boolean,
                    nombreCampoVigencia: string
  ) {
    let vigencia: ActivoInactivo = ActivoInactivo.ACTIVO;
    if (!habilitado) {
      vigencia = ActivoInactivo.INACTIVO;
    } else {
      vigencia = ActivoInactivo.ACTIVO;
    }
    this.cargandoService.habilitarCargando();
    const indice = this.arregloDatos
      .findIndex(
        (a: any) => a[this.servicio.nombreIdentificador] === idRegistro
      );
    this.servicio
      .setearVigencia(
        vigencia,
        idRegistro
      )
      .subscribe(
        () => {
          this.cargandoService.deshabilitarCargando();
          const objeto = this.arregloDatosFiltrado[indice] as any;
          objeto[nombreCampoVigencia] = vigencia;
          this.arregloDatosFiltrado[indice] = objeto;
          this.arregloDatos[indice] = objeto;
          this.notificacionService.anadir({
            titulo: 'Éxito',
            detalle: 'Se modifico la vigencia',
            severidad: 'info'
          });
        },
        (error) => {
          const objeto = this.arregloDatosFiltrado[indice] as any;
          objeto.habilitado = !habilitado;
          this.cargandoService.deshabilitarCargando();
          console.error({
            mensaje: 'Error cargando registros',
            data: this.busquedaDto,
            error
          });
          this.notificacionService.anadir({
            titulo: 'Error',
            detalle: 'Error del servidor',
            severidad: 'error'
          });
          this.cargandoService.deshabilitarCargando();
        }
      );
  }

  buscar(): Observable<[Entidad[], number]> {
    if (this.busquedaDto) {
      const busqueda = this.busquedaDto as any;
      this.skip = busqueda.skip;
      this.take = busqueda.take;
      return this.servicio.buscar(
        this.busquedaDto
      )
        .pipe(
          map(
            resultados => {
              const arreglo = resultados[0].map(
                (r: any) => {
                  if (r[this.servicio.nombreCampoVigencia] === ActivoInactivo.ACTIVO) {
                    r.habilitado = true;
                  } else {
                    r.habilitado = false;
                  }
                  return r;
                });
              this.totalRecords = resultados[1];
              this.arregloDatos = [...arreglo];
              this.arregloDatosFiltrado = [...arreglo];
              return [[...arreglo], this.totalRecords];
            }
          )
        );
    } else {
      this.totalRecords = 0;
      return of([[], 0]);
    }
  }

  aplicarFiltro(
    event: Event,
    nombreCampo: string[]
  ): void {
    const valorFiltro: string = (event.target as HTMLInputElement).value;
    this.arregloDatosFiltrado = aplicarFiltro(
      valorFiltro,
      nombreCampo,
      this.arregloDatos,
      this.arregloDatosFiltrado,
    );
  }

  limpiarFiltroGeneral(): void {
    this.filtroGeneralTabla = '';
    this.arregloDatosFiltrado = [...this.arregloDatos];
  }

  limpiarVariablesParaNuevaBusqueda(): void {
    this.arregloDatos = [];
    this.arregloDatosFiltrado = [];
  }

  cargarMasDatos(evento: { first: number, rows: number }): void {
    if (this
      .busquedaDto) {
      const busqueda = this.busquedaDto as any;
      busqueda.skip = (evento.first);
      this.busquedaDto = busqueda;
      this.cargandoService.habilitarCargando();
      this
        .buscar()
        .subscribe(
          () => {
            this.cargandoService.deshabilitarCargando();
            this.notificacionService.anadir({
              titulo: 'Éxito',
              detalle: 'Cargó registros',
              severidad: 'info'
            });
          },
          (error) => {
            console.error({
              mensaje: 'Error cargando registros',
              data: this.busquedaDto,
              error
            });
            this.notificacionService.anadir({
              titulo: 'Error',
              detalle: 'Error del servidor',
              severidad: 'error'
            });
            this.cargandoService.deshabilitarCargando();
          }
        );
    }
  }
}
