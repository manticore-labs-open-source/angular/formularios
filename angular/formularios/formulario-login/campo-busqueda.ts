import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_BUSQUEDA: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
    return {
        componente: claseComponente,
        validators: [
            Validators.required,
        ],
        asyncValidators: null,
        valorInicial: undefined,
        nombreCampo: 'busqueda',
        nombreMostrar: 'Busqueda',
        textoAyuda: 'Selecciona una opcion de busqueda.',
        placeholderEjemplo: 'Ej: opcion 1',
        formulario: {},
        mensajes: MENSAJES_ERROR(claseComponente),
        parametros: {
            nombreCampo: 'busqueda',
        },
        estaValido: false,
        tipoCampoHtml: 'autocomplete',
        valorActual: '',
        tamanioColumna: 6,
        disabled: false,
        hidden: false,
        autocomplete: {
            suggestions: [],
            field: 'title',
            delay: 2000,
            inputId: 'userId',
            emptyMessage: 'No hay registros',
            fnMostrarEnAutoComplete: (campo) => {
                return campo.title;
            }
        },
    };
};
