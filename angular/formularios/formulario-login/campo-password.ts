import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_PASSWORD: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [
      Validators.required,
      Validators.minLength(6)
    ],
    asyncValidators: null,
    valorInicial: '',
    nombreCampo: 'password',
    nombreMostrar: 'Password',
    textoAyuda: 'Ingresa tu password.',
    placeholderEjemplo: 'Ej: ***********',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'Password',
      minlength: 6,
    },
    estaValido: false,
    tipoCampoHtml: 'password',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false
  };
};
