import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CEDULA: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [
      Validators.required,
      Validators.minLength(11),
    ],
    asyncValidators: null,
    valorInicial: '',
    nombreCampo: 'cedula',
    nombreMostrar: 'Cédula',
    textoAyuda: 'Ingresa tu cédula.',
    placeholderEjemplo: 'Ej: 17-18120000',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'cedula',
      minlength: 11,
    },
    estaValido: false,
    tipoCampoHtml: 'inputMask',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false,
    inputMask: {
      mask: '99-99999999',
      slotChar: '_',
      characterPattern: [],
      fnValidar: (campo, componente) => {
        const numeros = campo.replaceAll('_', '').replaceAll('-', '');
        return numeros.length >= 10;
      }
    }
  };
};
