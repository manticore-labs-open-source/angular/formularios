import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_SUELDO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [
      Validators.required,
    ],
    asyncValidators: null,
    valorInicial: '',
    nombreCampo: 'sueldo',
    nombreMostrar: 'Sueldo',
    textoAyuda: 'Ingresa tu sueldo.',
    placeholderEjemplo: 'Ej: $99.231 USD',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'sueldo',
    },
    estaValido: false,
    tipoCampoHtml: 'inputNumber',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false,
    inputNumber: {
      max: 100,
      min: 0,
      prefix: '$  ',
      suffix: ' USD',
      minFractionDigits: 4,
      fnValidar: (campo, componente) => {
        return true;
      }
    }
  };
};
