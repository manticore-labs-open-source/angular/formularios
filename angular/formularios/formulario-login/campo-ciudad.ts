import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CIUDAD: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [
      Validators.required
    ],
    asyncValidators: null,
    valorInicial: '',
    nombreCampo: 'ciudad',
    nombreMostrar: 'Ciudad',
    textoAyuda: 'Ingresa tu ciudad.',
    placeholderEjemplo: 'Ej: Quito',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'ciudad',
    },
    estaValido: false,
    tipoCampoHtml: 'select',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false,
    select: {
      valoresSelect: [
        {nombre: 'Quito', id: 1},
        {nombre: 'Guayaquil', id: 1}
      ],
      placeholderFiltro: 'Filtre por nombre',
      mensajeFiltroVacio: 'No se encontró',
      campoFiltrado: 'nombre',
      fnMostrarEnSelect: (campo: any) => {
        return campo.nombre
      },
      campoSeleccionado: 'nombre'
    }
  };
};
