import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CORREO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [
      Validators.required,
      Validators.email
    ],
    asyncValidators: null,
    valorInicial: 'eadepto',
    nombreCampo: 'correo',
    nombreMostrar: 'Correo',
    textoAyuda: 'Ingresa tu correo.',
    placeholderEjemplo: 'Ej: info@manticore-labs.com',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'correo',
    },
    estaValido: false,
    tipoCampoHtml: 'text',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false
  };
};
