import {Validators} from '@angular/forms';
import {MENSAJES_ERROR} from '../../constantes/formulario/mensajes-error';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const CAMPO_CASADO: (claseComponente: any) => CampoFormulario = (claseComponente: any) => {
  return {
    hidden: false,
    componente: claseComponente,
    validators: [
      Validators.required,
    ],
    asyncValidators: null,
    valorInicial: true,
    nombreCampo: 'casado',
    nombreMostrar: 'Casado',
    textoAyuda: 'Seleccion si está casado.',
    placeholderEjemplo: 'Ej: Si/No',
    formulario: {},
    mensajes: MENSAJES_ERROR(claseComponente),
    parametros: {
      nombreCampo: 'casado',
    },
    estaValido: false,
    tipoCampoHtml: 'inputSwitch',
    valorActual: '',
    tamanioColumna: 6,
    disabled: false,
  };
};
