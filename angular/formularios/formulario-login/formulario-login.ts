import {CAMPO_CORREO} from './campo-correo';
import {CAMPO_PASSWORD} from './campo-password';
import {CAMPO_CIUDAD} from './campo-ciudad';
import {CAMPO_CEDULA} from './campo-cedula';
import {CAMPO_SUELDO} from './campo-sueldo';
import {CAMPO_CASADO} from './campo-casado';
import {CAMPO_BUSQUEDA} from './campo-busqueda';
import {CampoFormulario} from '../../componentes/ml-campo-formulario/interfaces/campo-formulario';

export const FORMULARIO_LOGIN: (claseComponente: any) => CampoFormulario[] = (claseComponente: any) => {
  return [
    CAMPO_CORREO(claseComponente),
    CAMPO_PASSWORD(claseComponente),
    CAMPO_CIUDAD(claseComponente),
    CAMPO_CEDULA(claseComponente),
    CAMPO_SUELDO(claseComponente),
    CAMPO_CASADO(claseComponente),
    CAMPO_BUSQUEDA(claseComponente),
  ];
};
