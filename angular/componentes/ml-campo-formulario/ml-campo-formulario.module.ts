  import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MlCampoFormularioComponent} from './ml-campo-formulario/ml-campo-formulario.component';
import {MlAyudaFormModule} from '../ml-ayuda-form/ml-ayuda-form.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MlErrorFormModule} from '../ml-error-form/ml-error-form.module';
import {MlContenedorCamposFormularioComponent} from './ml-contenedor-campos-formulario/ml-contenedor-campos-formulario.component';
import {DropdownModule} from 'primeng/dropdown';
import {InputMaskModule} from 'primeng/inputmask';
import {InputNumberModule} from 'primeng/inputnumber';
import {InputSwitchModule} from 'primeng/inputswitch';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';


@NgModule({
  declarations: [
    MlCampoFormularioComponent,
    MlContenedorCamposFormularioComponent,
  ],
  exports: [
    MlCampoFormularioComponent,
    MlContenedorCamposFormularioComponent,
  ],
  imports: [
    CommonModule,
    MlAyudaFormModule,
    ReactiveFormsModule,
    FormsModule,
    MlErrorFormModule,
    DropdownModule,
    InputMaskModule,
    InputNumberModule,
    InputSwitchModule,
    AutoCompleteModule,
    FormsModule,
    ButtonModule,
    RippleModule
  ]
})
export class MlCampoFormularioModule {
}
