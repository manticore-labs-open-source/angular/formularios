import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CampoFormulario} from '../interfaces/campo-formulario';
import {EventoCambioFormulario} from '../interfaces/evento-cambio-formulario';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TodosCamposValidados} from '../interfaces/todos-campos-validados';
import {EventoCambioAutocomplete} from '../interfaces/evento-cambio-autocomplete';

@Component({
  selector: 'app-ml-contenedor-campos-formulario',
  templateUrl: './ml-contenedor-campos-formulario.component.html',
  styleUrls: ['./ml-contenedor-campos-formulario.component.scss'],
})
export class MlContenedorCamposFormularioComponent implements OnInit {


  formulario?: FormGroup;

  @Input()
  camposFormularioFuncion: (claseComponente: any) => CampoFormulario[] = () => []

  @Input()
  componente: any;

  @Output()
  todosCamposValidos: EventEmitter<TodosCamposValidados> = new EventEmitter<TodosCamposValidados>();

  camposFormulario: CampoFormulario[] = [];

  @Output()
  cambioCampo: EventEmitter<EventoCambioFormulario> = new EventEmitter<EventoCambioFormulario>();

  @Output()
  cambioAutocomplete: EventEmitter<EventoCambioAutocomplete> = new EventEmitter<EventoCambioAutocomplete>();

  formularioListo = false;

  constructor(
    private readonly formBuilder: FormBuilder,
  ) {

  }

  ngOnInit() {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.camposFormulario = this.camposFormularioFuncion(this.componente);
    let objetoConstrolsConfig: any = {};
    this.camposFormulario
      .filter((c) => !c.hidden)
      .forEach(
        (campo) => {
          if (campo.validators) {
            const objeto = {
              [campo.nombreCampo]: new FormControl(
                {value: campo.valorInicial, disabled: campo.disabled},
                [...campo.validators]
              )
            };
            objetoConstrolsConfig = {
              ...objetoConstrolsConfig,
              ...objeto
            };
          }


        }
      );
    this.formulario = this.formBuilder
      .group(
        objetoConstrolsConfig
      );
    this.camposFormulario.forEach(
      (campo) => {
        campo.formulario = this.formulario;
      }
    );
    this.formularioListo = true;
  }

  cambioCampoEvento(evento: EventoCambioFormulario, indiceArreglo: number) {
    this.cambioCampo.emit(evento);
    this.camposFormulario[indiceArreglo].valorActual = evento.valor;
    this.camposFormulario[indiceArreglo].estaValido = evento.estaValido;
    this.validarTodosCamposValidados();
  }

  validarTodosCamposValidados() {
    if (this.formulario) {
      setTimeout(
        () => {
          if (this.formulario) {
            const todosEstanValidos = this.camposFormulario
              .filter((c) => !c.hidden)
              .filter((c) => !c.disabled)
              .every((c) => c.estaValido) && this.formulario.valid;
            this.todosCamposValidos.emit(
              {
                valido: todosEstanValidos,
                camposFormulario: this.camposFormulario
              }
            );
          }

        },
        1
      )

    }
  }

  cambioCampoAutocomplete(evento: EventoCambioAutocomplete, indiceArreglo: number) {
    this.cambioAutocomplete.emit({
      ...evento,
      indiceArreglo,
    });
  }

}
