import {AsyncValidatorFn, FormGroup, ValidatorFn} from '@angular/forms';
import {FormularioErrorMensaje} from '../../ml-error-form/interfaces/formulario-error-mensaje';
import {ParametrosErrorFormulario} from '../../ml-error-form/interfaces/parametros-error-formulario';

export interface CampoFormulario {
  componente: any;
  validators?: ValidatorFn[];
  asyncValidators?: AsyncValidatorFn | AsyncValidatorFn[] | null;
  valorInicial: any;
  nombreCampo: string;
  nombreMostrar: string;
  tipoCampoHtml: 'password' | 'text' | 'select' | 'inputMask' | 'inputNumber' | 'inputSwitch' | 'autocomplete';
  textoAyuda: string;
  placeholderEjemplo: string;
  formulario: FormGroup | any;
  hidden: boolean;
  mensajes: FormularioErrorMensaje[];
  parametros: ParametrosErrorFormulario;
  estaValido: boolean;
  valorActual: any;
  tamanioColumna: number;
  disabled: boolean;
  select?: {
    valoresSelect: any[];
    campoFiltrado: string;
    campoSeleccionado: string;
    placeholderFiltro: string;
    mensajeFiltroVacio: string;
    fnMostrarEnSelect: (campo: any) => string;
  },
  inputMask?: {
    mask: string;
    characterPattern: any;
    slotChar: string;
    fnValidar: (campo: any, componente: any) => boolean;
  },
  inputNumber?: {
    min: number;
    max: number;
    prefix: string;
    suffix: string;
    minFractionDigits: number;
    fnValidar: (campo: any, componente: any) => boolean;
  },
  autocomplete?: {
    suggestions: any[];
    field: any;
    delay: number;
    emptyMessage: string;
    fnMostrarEnAutoComplete: (campo: any) => string;
    inputId: string;
  },
}
