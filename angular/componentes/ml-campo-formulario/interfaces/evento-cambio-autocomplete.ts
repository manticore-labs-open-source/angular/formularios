import {CampoFormulario} from './campo-formulario';

export interface EventoCambioAutocomplete {
  campoFormulario: CampoFormulario;
  originalEvent: InputEvent;
  query: string;
  indiceArreglo: number;
}
