import {CampoFormulario} from './campo-formulario';

export interface TodosCamposValidados {
    valido: boolean;
    camposFormulario: CampoFormulario[];
}
