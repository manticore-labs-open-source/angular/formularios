import {CampoFormulario} from './campo-formulario';

export interface EventoCambioFormulario {
    campoFormulario?: CampoFormulario;
    evento: any;
    valor: any;
    estaValido: boolean;
}
