import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CampoFormulario} from '../interfaces/campo-formulario';
import {FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {EventoCambioFormulario} from '../interfaces/evento-cambio-formulario';
import {EventoCambioAutocomplete} from '../interfaces/evento-cambio-autocomplete';

@Component({
  selector: 'app-ml-campo-formulario',
  templateUrl: './ml-campo-formulario.component.html',
  styleUrls: ['./ml-campo-formulario.component.scss'],
})
export class MlCampoFormularioComponent implements OnInit {

  @Input()
  campoFormulario?: CampoFormulario;

  esRequired = false;

  @Output()
  cambioCampo: EventEmitter<EventoCambioFormulario> = new EventEmitter<EventoCambioFormulario>();

  @Output()
  cambioAutocomplete: EventEmitter<EventoCambioAutocomplete> = new EventEmitter<EventoCambioAutocomplete>();

  constructor() {
  }

  ngOnInit() {
    if (this.campoFormulario) {
      const validadores = this.campoFormulario.validators as ValidatorFn[];
      if (validadores.length > 0) {
        this.esRequired = validadores.some((v) => v === Validators.required);
      }
      if (this.campoFormulario) {
        const f = this.campoFormulario
          .formulario as FormGroup;
        const componente = f
          .get(this.campoFormulario.nombreCampo)
        if (componente) {
          if (this.campoFormulario.valorInicial) {
            this.campoFormulario.estaValido = componente.valid;
          }
          componente
            .valueChanges
            .subscribe(
              (evento: any) => {
                if (this.campoFormulario) {
                  switch (this.campoFormulario.tipoCampoHtml) {
                    case 'inputNumber':
                      if (this.campoFormulario.tipoCampoHtml === 'inputNumber'
                        && this.campoFormulario.inputNumber) {
                        const estaValido = this.campoFormulario.inputNumber.fnValidar(evento, this.campoFormulario.componente);
                        if (estaValido) {
                          this.campoFormulario.estaValido = true;
                          f.controls[this.campoFormulario.nombreCampo].setErrors(null);
                          this.cambioCampo.emit({
                            campoFormulario: this.campoFormulario,
                            evento,
                            valor: evento,
                            estaValido: true,
                          });
                        } else {
                          const f = this.campoFormulario.formulario as FormGroup;
                          this.campoFormulario.estaValido = false;
                          f.controls[this.campoFormulario.nombreCampo].setErrors({});
                          this.cambioCampo.emit({
                            campoFormulario: this.campoFormulario,
                            evento: undefined,
                            valor: undefined,
                            estaValido: false,
                          });
                        }
                      }
                      break;
                    case 'inputMask':
                      if (this.campoFormulario.tipoCampoHtml === 'inputMask'
                        && this.campoFormulario.inputMask) {
                        const estaValido = this.campoFormulario.inputMask.fnValidar(evento, this.campoFormulario.componente);
                        if (estaValido) {
                          this.campoFormulario.estaValido = true;
                          f.controls[this.campoFormulario.nombreCampo].setErrors(null);
                          this.cambioCampo.emit({
                            campoFormulario: this.campoFormulario,
                            evento,
                            valor: evento,
                            estaValido: true,
                          });
                        } else {
                          const f = this.campoFormulario.formulario as FormGroup;
                          this.campoFormulario.estaValido = false;
                          f.controls[this.campoFormulario.nombreCampo].setErrors({});
                          this.cambioCampo.emit({
                            campoFormulario: this.campoFormulario,
                            evento: undefined,
                            valor: undefined,
                            estaValido: false,
                          });
                        }
                      }
                      break;
                    default:
                      this.campoFormulario.estaValido = componente.valid
                      this.cambioCampo.emit({
                        campoFormulario: this.campoFormulario,
                        evento,
                        valor: evento,
                        estaValido: componente.valid
                      });
                      break;
                  }

                }

              })
        }
      }
    }
  }

  cambio(evento: { originalEvent: InputEvent, query: string }) {
    if (this.campoFormulario) {
      const f = this.campoFormulario
        .formulario as FormGroup;
      const componente = f
        .get(this.campoFormulario.nombreCampo)
      if (componente) {
        this.campoFormulario.estaValido = componente.valid
        if (this.campoFormulario) {
          this.cambioAutocomplete.emit({
            ...evento,
            indiceArreglo: 0,
            campoFormulario: this.campoFormulario,
          });
        }
      }
    }
  }

  empezoAEscribir() {
    if (this.campoFormulario) {
      if (this.campoFormulario.formulario) {
        const f = this.campoFormulario.formulario as FormGroup
        f.controls[this.campoFormulario.nombreCampo].setErrors({});
        this.cambioCampo.emit({
          campoFormulario: this.campoFormulario,
          evento: undefined,
          valor: undefined,
          estaValido: this.campoFormulario.formulario.valid,
        });
      }
    }
  }

  limpiarCampo(){
    if (this.campoFormulario) {
      const f = this.campoFormulario
        .formulario as FormGroup;
      const componente = f
        .get(this.campoFormulario.nombreCampo)
      if (componente) {
        this.campoFormulario.estaValido = false;
        f.controls[this.campoFormulario.nombreCampo].setErrors({});
        f.controls[this.campoFormulario.nombreCampo].patchValue(undefined);
        this.campoFormulario.estaValido = false;
        this.cambioCampo.emit({
          campoFormulario: this.campoFormulario,
          evento: undefined,
          valor: undefined,
          estaValido: this.campoFormulario.formulario.valid,
        });
      }
    }
  }

}
