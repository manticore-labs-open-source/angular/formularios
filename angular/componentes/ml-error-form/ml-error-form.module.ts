import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MlErrorFormComponent} from './ml-error-form/ml-error-form.component';

@NgModule({
  declarations: [
    MlErrorFormComponent
  ],
  exports: [
    MlErrorFormComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MlErrorFormModule {
}
