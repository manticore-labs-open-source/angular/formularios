export interface ParametrosErrorFormulario {
    nombreCampo: string;
    minlength?: number;
    maxlength?: number;
    min?: number;
    max?: number;
    mensajePattern?: string;
}
