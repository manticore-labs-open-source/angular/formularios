import {ParametrosErrorFormulario} from './parametros-error-formulario';

export interface FormularioErrorMensaje {
    tipo: 'required' | 'minlength' | 'maxlength' | 'pattern' | 'email' | 'min' | 'max';
    mensaje: (parametros: ParametrosErrorFormulario) => string;
}
