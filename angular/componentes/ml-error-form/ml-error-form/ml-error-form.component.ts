import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormularioErrorMensaje} from '../interfaces/formulario-error-mensaje';
import {ParametrosErrorFormulario} from '../interfaces/parametros-error-formulario';

@Component({
    selector: 'app-ml-error-form',
    templateUrl: './ml-error-form.component.html',
    styleUrls: ['./ml-error-form.component.scss'],
})
export class MlErrorFormComponent implements OnInit {

    @Input()
    formulario?: FormGroup | any;

    @Input()
    mensajes: FormularioErrorMensaje[] = [];

    @Input()
    nombreCampo = '';

    @Input()
    parametros: ParametrosErrorFormulario = {nombreCampo: ''};

    constructor() {
    }

    ngOnInit() {
    }

}
