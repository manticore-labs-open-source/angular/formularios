import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MlAyudaFormComponent} from './ml-ayuda-form/ml-ayuda-form.component';

@NgModule({
  declarations: [
    MlAyudaFormComponent
  ],
  exports: [
    MlAyudaFormComponent
  ],
  imports: [
    CommonModule,
  ],
})
export class MlAyudaFormModule {
}
