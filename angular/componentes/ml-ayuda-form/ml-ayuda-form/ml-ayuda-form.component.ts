import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ml-ayuda-form',
  templateUrl: './ml-ayuda-form.component.html',
  styleUrls: ['./ml-ayuda-form.component.scss'],
})
export class MlAyudaFormComponent implements OnInit {

  @Input()
  ayuda = '';

  @Input()
  id = '';

  constructor() {
  }

  ngOnInit() {
  }

}
