export interface ServicioComun {
  modificarVigencia: (id: string, objeto: any) => any;
  crear: (objeto: any) => any;
  actualizarPorId: (id: string, objeto: any) => any;

  [key: string]: any;
}
