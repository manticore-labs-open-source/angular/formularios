import { IsNumberString, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';

export abstract class BusquedaComunDto {
  @IsOptional()
  @IsNumberString()
  @Expose()
  skip: string;

  @IsOptional()
  @IsNumberString()
  @Expose()
  take: string;
}
