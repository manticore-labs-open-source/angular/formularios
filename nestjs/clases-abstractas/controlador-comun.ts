import {
  Body,
  HttpCode,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { SeguridadGuard } from '../guards/seguridad/seguridad.guard';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { ObjetoControladorComun } from './interfaces/objeto-controlador-comun';
import { AuditoriaService } from '../servicios/auditoria/auditoria.service';
import { Errores } from '../enums/errores';
import { ServicioComun } from './interfaces/servicio-comun';
import { FindConditions, FindManyOptions } from 'typeorm';
import { ServicioLogisticoEntity } from '../servicio-logistico/servicio-logistico.entity';
import { NUMERO_MAXIMO_REGISTROS_A_CONSULTARSE } from '../constantes/numero-maximo-registros-a-consultarse';
import { ActivoInactivo } from '../enums/activo-inactivo';

export abstract class ControladorComun {
  constructor(
    private readonly _servicio: ServicioComun,
    private readonly auditoriaService: AuditoriaService,
    private readonly _objetoControladorComun: ObjetoControladorComun,
  ) {}

  @Put(':id/modificar-vigencia')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async modificarVigencia(
    @Param('id') id: string,
    @Body() body: any,
    @Req() request,
  ) {
    const respuesta = await this.validarRespuesta(
      request,
      body,
      this._objetoControladorComun.modificarVigencia.dto,
      this._objetoControladorComun.modificarVigencia.mensaje,
      id,
    );
    if (respuesta === 'ok') {
      return this._servicio.modificarVigencia(id, body);
    }
  }

  @Post('')
  @HttpCode(201)
  @UseGuards(SeguridadGuard)
  async crear(@Body() body: any, @Req() request) {
    const respuesta = await this.validarRespuesta(
      request,
      body,
      this._objetoControladorComun.crearEntidad.dto,
      this._objetoControladorComun.crearEntidad.mensaje,
    );
    if (respuesta === 'ok') {
      return this._servicio.crear(body);
    }
  }

  @Put(':id')
  @HttpCode(200)
  @UseGuards(SeguridadGuard)
  async actualizar(@Param('id') id: string, @Body() body: any, @Req() request) {
    const respuesta = await this.validarRespuesta(
      request,
      body,
      this._objetoControladorComun.actualizarEntidad.dto,
      this._objetoControladorComun.actualizarEntidad.mensaje,
      id,
    );
    if (respuesta === 'ok') {
      return this._servicio.actualizarPorId(id, body);
    }
  }

  protected setearSkipYTake<T>(
    consulta: FindManyOptions<T>,
    skip?: string,
    take?: string,
  ) {
    if (take && Number(take) <= NUMERO_MAXIMO_REGISTROS_A_CONSULTARSE) {
      consulta.take = Number(take);
    } else {
      consulta.take = 10;
    }
    if (skip !== undefined) {
      consulta.skip = Number(skip);
    } else {
      consulta.skip = 0;
    }
    return consulta;
  }

  protected setearFiltro<T>(
    nombreCampoFiltro: string,
    where: FindConditions<T>[],
    valorVigencia?: any,
  ) {
    if (valorVigencia) {
      if (where.length === 0) {
        const data: any = {};
        data[nombreCampoFiltro] = valorVigencia;
        where.push(data);
      } else {
        where = where.map((data) => {
          const datosModificados: any = {
            ...data,
          };
          datosModificados[nombreCampoFiltro] = valorVigencia;
          return datosModificados;
        });
      }
    }
    return where;
  }

  validarBusquedaDto(request: any, parametrosConsulta: any) {
    return this.validarRespuesta(
      request,
      parametrosConsulta,
      this._objetoControladorComun.busqueda.dto,
      this._objetoControladorComun.busqueda.mensaje,
    );
  }

  protected async validarRespuesta(
    request: any,
    body: any,
    dto: any,
    mensaje: any,
    id: any = null,
    error: Errores = Errores.BAD_REQUEST,
  ): Promise<'ok'> {
    const object = plainToClass(dto, body, {
      excludeExtraneousValues: true,
    });
    const errors = await validate(object);
    if (errors.length > 0) {
      this.auditoriaService.error(
        request,
        {
          data: id ? { ...body, id } : body,
          errors,
        },
        error,
        mensaje,
      );
    } else {
      return 'ok';
    }
  }
}
