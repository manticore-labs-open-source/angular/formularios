import {FindManyOptions, Repository} from 'typeorm';

export abstract class ServicioComun<Entidad> {
    nombreCampoId = '';

    constructor(
        private repositorio: Repository<Entidad>,
        nombreCampoId: string
    ) {
        this.nombreCampoId = nombreCampoId;

    }

    crear(objeto: Entidad | any) {
        return this.repositorio.save<Entidad>(objeto);
    }

    async actualizarPorId(id: string, objeto: Entidad | any) {
        objeto.pene = undefined
        const registro = await this.repositorio.findOneOrFail(id);
        Object
            .keys(objeto)
            .map((a) => {
                if (objeto[a] === undefined) {
                    delete objeto[a]
                }
            });
        let objetoAModificar: any = {...registro, ...objeto};
        return this.repositorio.save<Entidad>(objetoAModificar);
    }

    async modificarVigencia(id: string, objeto: Entidad | any) {
        const registro = await this.repositorio.findOneOrFail(id);
        let objetoAModificar: any = {...registro, ...objeto};
        return this.repositorio.save<Entidad>(objetoAModificar);
    }

    buscar(consulta: FindManyOptions<Entidad>) {
        return this.repositorio.findAndCount(consulta);
    }
}
